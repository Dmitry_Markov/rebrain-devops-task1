# rebrain-devops-task1

Default configuration file for Nginx web server.

## Usage

Use as a starting poit for your own configuration file for Nginx servers.

## API

API is under construction.

## Install

TBD.

## Acknowledgments

rebrain-devops-task1 is a part of Rebrain DevOps course.

## See Also

- [Full example configuration](https://www.nginx.com/resources/wiki/start/topics/examples/full/)

## License

ISC
